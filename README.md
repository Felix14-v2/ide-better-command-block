<span align="center">
<img src="https://gitlab.com/scs_torleon/hub-awesome-dungeon/-/raw/main/assets/BH_JTL_Header2.png"/>
<a title="Bisec Hosting | Official Partener 25% off !!!" href="https://www.bisecthosting.com/jtlelisa"><img src="https://gitlab.com/scs_torleon/hub-awesome-dungeon/-/raw/main/assets/bisec.png"/></a>

<a title="Support us patreon" href="https://www.patreon.com/jtorleon"><img src="https://img.shields.io/endpoint.svg?url=https://shieldsio-patreon.vercel.app/api?username=JTorLeon&amp;type=patrons&amp;style=for-the-badge" /></a> &nbsp;<a title="Join the discord !" href="https://discord.gg/UfZpVffM8X"><img src="https://img.shields.io/discord/908081566163554335?color=blueviolet&amp;label=Discord&amp;logo=Discord&amp;logoColor=blueviolet&amp;style=for-the-badge" /></a> &nbsp;<a href="https://twitter.com/JTorLeonCurse"><img src="https://img.shields.io/twitter/follow/JTorLeonCurse?color=blue&amp;label=Follow%20Me&amp;logo=Twitter&amp;style=for-the-badge"/></a> &nbsp;<a href="https://www.curseforge.com/minecraft/modpacks/quick-builder-pack-fabric"><img src="https://img.shields.io/badge/My-Modpacks-important?style=for-the-badge&amp;logo=curseforge&amp;logoColor=important"/></a>

</span>

# 📜 HUB what is it ? Where am I ?

This is the center of the project, the heart! You will find all the information you need. 

You won't find any source code on this Gitlab. Why not? The reason is simple, I decided to choose an "All Rights Reserved" license, it is useless to publish sources because no external help is possible with this license.


➔      🔗         CurseForge page project                             ⯈ [Fabric: CLICK HERE](https://www.curseforge.com/minecraft/mc-mods/ide-better-command-block-fabric-editor) ⯇⯈ [Forge: CLICK HERE](https://www.curseforge.com/minecraft/mc-mods/ide-better-command-block-forge-editor) ⯇ <br>
➔      🔗         PlanetMinecraft page project               ⯈ [CLICK HERE](https://www.planetminecraft.com/mod/better-command-block-ide-forge/) ⯇ <br>
➔      🔗         MinecraftForum post                                                 ⯈ [CLICK HERE](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/3122341-command-block-editor-fabric-forge-1-16-5-1-17-1-1) ⯇ <br>
➔      🔗         Discord project                                                           ⯈ [CLICK HERE](https://discord.gg/UfZpVffM8X) ⯇ <br>
➔      🔗         Wiki help project                                                      ⯈ [CLICK HERE](https://gitlab.com/scs_torleon/ide-better-command-block/-/wikis/home) ⯇ <br>

## 📜 Project description

This mod adds a multi-line editor for the "command block". You can edit multiple "command blocks" with an intuitive tab system.  You can write commands on 1,2,3... line (max line supported = memory limit).

Everything is 100% vanilla compatible, it's not a new programming language or anything. I just added the constant and comment principle, because it was very practical

- Execute the command block with the right click (netherite_hoe)
- Destroy command blocks only with a pickaxe (netherite_pickaxe)
- Open the BetterCommand Block IDE with the right click (wooden_shovel)

🗒️ See configuration to enable / disable features and change item names

⚠️ Check out the images/screenshots on CurseForge 🖼️ to see more! 

## ⭐ Please support me on Patreon if you like my content!

<a title="Please support me on Patreon if you like my content!" href="https://www.patreon.com/jtorleon"><img src="https://gitlab.com/scs_torleon/hub-awesome-dungeon/-/raw/main/assets/footer-patreon.png"></img></a>

Many thanks to my patrons:

🏆 Telum ⭐⭐⭐

🏆 Cr4nK ⭐⭐

🏆 Kaworru ⭐

🏆 EternalOne ⭐

🏆 Unheard ⭐

🏆 M Bochem ⭐

🏆 Marion ⭐

🏆 bonicki ⭐

🏆 Tosche Station [|🔋|] ⭐

🏆 theodore ඞ ⭐

🏆 EbolaChansoKawaii

🏆 Tishbyte

[your pseudo here](https://www.patreon.com/jtorleon)

<span align="center">

<a title="Join patreon!!!" href="https://www.patreon.com/jtorleon"><img src="https://gitlab.com/scs_torleon/hub-awesome-dungeon/-/raw/main/assets/adhere.png"/></a>
<a title="Bisec Hosting | Official Partener 25% off !!!" href="https://www.bisecthosting.com/jtlelisa"><img src="https://gitlab.com/scs_torleon/hub-awesome-dungeon/-/raw/main/assets/bisec.png"/></a>

</span>
